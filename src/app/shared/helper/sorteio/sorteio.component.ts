import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { DataApiService } from '../../service/data-api.service';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-sorteio',
  templateUrl: './sorteio.component.html',
  styleUrls: ['./sorteio.component.scss']
})
export class SorteioComponent implements OnInit {

  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[];
  sorteio: any;
  gallery: boolean;
  today: any;
  constructor(private db: DataApiService, private datePipe: DatePipe) { }

  ngOnInit() {
    this.today = this.datePipe.transform(new Date(), 'MM/dd/yyyy');
    console.log(this.today);
    this.getData();
    this.galleryOptions = [
      {
        width: '100%',
        height: '600px',
        thumbnailsColumns: 4,
        imageAnimation: NgxGalleryAnimation.Rotate
      },
      {
        breakpoint: 800,
        width: '100%',
        height: '500px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false
      }
    ];
  }

  getData() {
    this.db.getRead('data').subscribe(data => {
      const rows = data[0];
      this.sorteio = rows.sorteio;
      this.galleryImages = rows.gallery;
      this.gallery = rows.setting.gallery;
    });
  }

  getPopPup(i: any) {
    this.db.getMessage(i);
  }
}
