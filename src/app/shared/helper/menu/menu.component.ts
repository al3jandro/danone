import { DataApiService } from './../../service/data-api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  url: any;
  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.api.getRead('data').subscribe(data => {
      this.url = data[0].menu;
    });
  }

}
