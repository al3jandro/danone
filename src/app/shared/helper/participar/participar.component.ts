import { DataApiService } from './../../service/data-api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-participar',
  templateUrl: './participar.component.html',
  styleUrls: ['./participar.component.scss']
})
export class ParticiparComponent implements OnInit {

  text: any;

  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.api.getRead('data').subscribe(data =>{
      this.text = data[0].participar;
    })
  }

}
