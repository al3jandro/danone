import { Component, OnInit } from '@angular/core';
import { DataApiService } from '../../service/data-api.service';

@Component({
  selector: 'app-produtos',
  templateUrl: './produtos.component.html',
  styleUrls: ['./produtos.component.scss']
})
export class ProdutosComponent implements OnInit {

  frisco: any;

  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.api.getRead('data').subscribe(data => {
      this.frisco = data[0].frisco;
    })
  }

}
