import { DataApiService } from './../../service/data-api.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-sorte',
  templateUrl: './sorte.component.html',
  styleUrls: ['./sorte.component.scss']
})
export class SorteComponent implements OnInit {

  sorte: any;

  constructor(private api: DataApiService) { }

  ngOnInit() {
    this.api.getRead('data').subscribe(data => {
      this.sorte = data[0].produtos;
    })
  }

}
