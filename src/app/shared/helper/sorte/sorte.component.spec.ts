import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SorteComponent } from './sorte.component';

describe('SorteComponent', () => {
  let component: SorteComponent;
  let fixture: ComponentFixture<SorteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SorteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SorteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
