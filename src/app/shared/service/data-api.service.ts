import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class DataApiService {

  constructor(private http: HttpClient) { }

  getRead(json: string): Observable<any> {
    return this.http.get(`./assets/json/${json}.json`);
  }

  getMessage(code: any) {
    Swal.fire({
      title: `Resultado Loteria Federal ${code.numero}`,
      html:
      `<hr>
      <h5 class="nome-gan b-06 mt-4">${code.nome}</h5>
      <p class="lead b-06">${code.condor}</p>`,
      width: 600,
      showCancelButton: false,
      showConfirmButton: false,
      showCloseButton: true,
    });
  }
}


