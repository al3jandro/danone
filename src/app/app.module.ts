import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgxGalleryModule } from 'ngx-gallery';
import { environment } from '../environments/environment';
import { HomeComponent } from './shared/layout/home/home.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { MomentModule } from 'ngx-moment';

import { FooterComponent } from './shared/helper/footer/footer.component';
import { DownloadComponent } from './shared/helper/download/download.component';
import { HeaderComponent } from './shared/helper/header/header.component';
import { MenuComponent } from './shared/helper/menu/menu.component';
import { SorteComponent } from './shared/helper/sorte/sorte.component';
import { ParticiparComponent } from './shared/helper/participar/participar.component';
import { ProdutosComponent } from './shared/helper/produtos/produtos.component';
import { SorteioComponent } from './shared/helper/sorteio/sorteio.component';
import { ScrollComponent } from './shared/util/scroll/scroll.component';
import { DatePipe } from '@angular/common';
import { AnimeComponent } from './shared/util/anime/anime.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    FooterComponent,
    DownloadComponent,
    HeaderComponent,
    MenuComponent,
    SorteComponent,
    ParticiparComponent,
    ProdutosComponent,
    SorteioComponent,
    ScrollComponent,
    AnimeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NgxGalleryModule,
    MomentModule,
    ScrollToModule.forRoot(),
    MDBBootstrapModule.forRoot(),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent],
  schemas: [ NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
